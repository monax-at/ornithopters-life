/**
 * Created by Sergey on 22.01.2017.
 */

function TestRender(scene) {
    this.graphics = new PIXI.Graphics();
    scene.addChild(this.graphics);

    this.clearColor = function() {
        this.graphics.clear();
        this.graphics.lineStyle(2, 0x0000FF, 1);
    }

    this.renderBox = function(box) {
        var first = null;
        for (var consoleIndex in box.vertices) {
            var vertex = box.vertices[consoleIndex];
            if (!first) {
                first = vertex;
                this.graphics.moveTo(vertex.x, vertex.y);
            } else {
                this.graphics.lineTo(vertex.x, vertex.y);
            }
        }
        if (first) {
            this.graphics.lineTo(first.x, first.y);
        }
    }
}