"use strict";

$(document).ready(function () {
    var app = new Game();
    app.init();
    app.start();
})