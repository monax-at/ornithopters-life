"use strict";

var windowSize = {
    width: 1360,
    height: 768
};

// module aliases
var Engine = Matter.Engine,
    World = Matter.World,
    Body = Matter.Body,
    Bodies = Matter.Bodies,
    Composite = Matter.Composite,
    Constraint = Matter.Constraint;

function sortDepht(a, b) {
    // if (a.z == undefined && b.z == undefined) {
    //     return 0;
    // }

    if (a.z < b.z) return -1;
    if (a.z > b.z) return 1;
    return 0;
}

function Game() {
    this.scene = null;
    this.render = null;
    this.engine = null;

    this.fpsFrameCounter = 0;
    this.fpsTimeCounter = 0;
    this.fpsOldTime = Date.now();
    this.fpsText = null;

    this.thopter = null;
    this.gameWorld = null;

    this.init = function() {
        console.log('Game init');

        //Create the renderer
        this.renderer = PIXI.autoDetectRenderer(windowSize.width, windowSize.height, {
            antialias: false, transparent: false, resolution: 1
        });

        //Add the canvas to the HTML document
        document.body.appendChild(this.renderer.view);

        //Create a container object called the `stage`
        this.scene = new PIXI.Container();

        this.engine = Engine.create(null, {});
        this.engine.world.gravity.y = 0;

        this.gameWorld = new GameWorld(windowSize, this.scene, this.engine);
        this.thopter = new Thopter(windowSize.width/2, windowSize.height/2, this.scene, this.engine);

        // fps counter
        this.fpsText = new PIXI.Text('0', {fontSize : '18px', fill : '#F7EDCA'});
        this.fpsText.x = 10;
        this.fpsText.y = 10;
        this.fpsText.z = 0;
        this.scene.addChild(this.fpsText);

        // fps counter
        var Text = new PIXI.Text('Controls - left and right keyboard arrows.', {fontSize : '14px', fill : '#F7EDCA'});
        Text.x = 10;
        Text.y = windowSize.height - 20;
        this.scene.addChild(Text);
    }

    this.countFps = function () {
        this.fpsFrameCounter++;
        var timeInMs = Date.now();
        // console.log(this.fpsTimeCounter);
        this.fpsTimeCounter += timeInMs - this.fpsOldTime;
        if (this.fpsTimeCounter >= 1000) {
            this.fpsTimeCounter = 0;
            this.fpsText.setText(this.fpsFrameCounter);
            this.fpsFrameCounter = 0;
        }
        this.fpsOldTime = timeInMs;
    }

    this.logic = function () {
        this.thopter.run();

        // update physics
        Engine.update(this.engine, 1000 / 60)
    }

    this.render = function () {
        this.gameWorld.draw();
        this.thopter.draw();
        //Render the stage to see the animation
        this.renderer.render(this.scene);
    }

    this.gameLoop = function() {
        //Loop this function at 60 frames per second
        requestAnimationFrame(this.gameLoop);
        this.countFps();

        this.logic();
        this.render();
    }.bind(this)

    this.start = function() {
        //Start the game loop
        this.gameLoop();
    }
}
