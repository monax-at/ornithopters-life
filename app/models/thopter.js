/**
 * Created by Sergey on 21.01.2017.
 */

"use strict";
var isCountAnimLeft = false;
var CountAnumLeftCounter = 0;
var isCountAnimRight = false;
var CountAnumRightCounter = 0;

var Thopter = function (x, y, graphicsScene, physicsEngine) {
    this.x = x;
    this.y = y;
    this.width = 48;
    this.height = 64;
    this.graphicsScene = graphicsScene; // pixi draw stage
    this.physicsEngine = physicsEngine;
    this.leftWingReload = 0;
    this.rightWingReload = 0;
    this.wingReloadTime = 20; // 3 times at 1 sec
    this.leftWingWave = false;
    this.rightWingWave = false;
    this.graphicsBody = null;

    var graphicsContainer = new PIXI.DisplayObjectContainer();
    graphicsContainer.x = this.x;
    graphicsContainer.y = this.y;


    var group = Body.nextGroup(true);
    // var bee = Composite.create({label: 'bee'});
    this.body = Bodies.rectangle(this.x, this.y, this.width, this.height, {
        density: 0.8,
        friction: 0.1,
        frictionAir: 0.025,
        collisionFilter: {
            group: group
        }
    });

    var wingOffsetX = this.width / 2 + 12;
    var wingOffsetY = 0;
    this.leftWing = Bodies.circle(this.x - wingOffsetX, this.y + wingOffsetY, 6, {
        collisionFilter: {group: group}
    });

    this.rightWing = Bodies.circle(this.x + wingOffsetX, this.y + wingOffsetY, 6, {
        collisionFilter: {group: group}
    });

    this.testRender = new TestRender(this.graphicsScene);

    var bee = Body.create({
        parts: [this.body, this.leftWing, this.rightWing]
    });

    // add all of the bodies to the world
    World.add(this.physicsEngine.world, [bee]);

    //Capture the keyboard arrow keys
    var left = keyboard(37),
        right = keyboard(39);
        //up = keyboard(38),
        //down = keyboard(40);

    left.press = function () {this.leftWingWave = true; }.bind(this)
    left.release = function () {this.leftWingWave = false; }.bind(this)
    right.press = function () {this.rightWingWave = true; }.bind(this)
    right.release = function () {this.rightWingWave = false; }.bind(this)

    this.draw = function () {
        this.testRender.clearColor();
        this.testRender.renderBox(this.body);
        this.testRender.renderBox(this.leftWing);
        this.testRender.renderBox(this.rightWing);
    }

    this.initGraphics = function() {
        //Create the `cat` sprite from the texture
        this.graphicsBody = new PIXI.Sprite(
            PIXI.loader.resources["orni-body"].texture
        );
        this.graphicsBody.anchor.x = 0.5;
        this.graphicsBody.anchor.y = 0.5;
        // this.graphicsBody.x = this.x;
        // this.graphicsBody.y = this.y;

        this.graphicsLeftWing = new PIXI.Sprite(
            PIXI.loader.resources["orni-wing-left"].texture
        );
        this.graphicsLeftWing.anchor.x = 0.75;
        this.graphicsLeftWing.anchor.y = 0.25;
        this.graphicsLeftWing.x =  - this.width /2 + 5;
        this.graphicsLeftWing.y = 0;

        this.graphicsRightWing = new PIXI.Sprite(
            PIXI.loader.resources["orni-wing-right"].texture
        );
        this.graphicsRightWing.anchor.x = 0.25;
        this.graphicsRightWing.anchor.y = 0.25;
        this.graphicsRightWing.x =  + this.width /2 - 5;
        this.graphicsRightWing.y = 0;

        graphicsContainer.addChild(this.graphicsLeftWing);
        graphicsContainer.addChild(this.graphicsRightWing);
        graphicsContainer.addChild(this.graphicsBody);

        this.graphicsScene.addChild(graphicsContainer);
    }.bind(this)

    this.loadDatas = function() {
        //Use Pixi's built-in `loader` object to load an image
        PIXI.loader
            .add('orni-body', "/images/orni-body.png")
            .add('orni-wing-left', "/images/orni-wing-left.png")
            .add('orni-wing-right', "/images/orni-wing-right.png")
            .load(this.initGraphics);
    }.bind(this)

    this.loadDatas();

    this.countAnim = function() {
        if (isCountAnimLeft) {
            CountAnumLeftCounter++;
            if (CountAnumLeftCounter <= 2) {
                this.graphicsLeftWing.rotation += 0.5;
            } else if (CountAnumLeftCounter > 5 && CountAnumLeftCounter <= 10 ) {

            } else if (CountAnumLeftCounter >= 10 && CountAnumLeftCounter <= 20 ) {
                this.graphicsLeftWing.rotation -= 0.1;
            } else if (CountAnumLeftCounter >= 20) {
                this.graphicsLeftWing.rotation = 0;
                CountAnumLeftCounter = 0;
                isCountAnimLeft = false;
            }
        }
        if (isCountAnimRight) {
            CountAnumRightCounter++;
            if (CountAnumRightCounter <= 2) {
                this.graphicsRightWing.rotation -= 0.5;
            } else if (CountAnumRightCounter > 5 && CountAnumRightCounter <= 10 ) {

            } else if (CountAnumRightCounter >= 10 && CountAnumRightCounter <= 20 ) {
                this.graphicsRightWing.rotation += 0.1;
            } else if (CountAnumRightCounter >= 20) {
                this.graphicsRightWing.rotation = 0;
                CountAnumRightCounter = 0;
                isCountAnimRight = false;
            }
        }
    }

    this.run = function() {
        this.leftWingReload++;
        this.rightWingReload++;

        if (this.leftWingWave && this.leftWingReload >= this.wingReloadTime) {
            this.leftWingReload = 0;
            var vector = {x: 1, y: -2};
            vector = Matter.Vector.rotate(vector, bee.angle);
            Matter.Body.applyForce(bee, this.leftWing.position, vector);

            this.graphicsLeftWing.rotation = 0;
            CountAnumLeftCounter = 0;
            isCountAnimLeft = true;
        }

        if (this.rightWingWave && this.rightWingReload >= this.wingReloadTime) {
            this.rightWingReload = 0;
            var vector = {x: -1, y: -2};
            vector = Matter.Vector.rotate(vector, bee.angle);
            Matter.Body.applyForce(bee, this.rightWing.position, vector);

            this.graphicsLeftWing.rotation = 0;
            CountAnumRightCounter = 0;
            isCountAnimRight = true;
        }

        this.countAnim();

        // graphicsContainer
        graphicsContainer.x = bee.position.x;
        graphicsContainer.y = bee.position.y;
        graphicsContainer.rotation = bee.angle;
    }
}
