/**
 * Created by Sergey on 21.01.2017.
 */

"use strict";

var GameWorld = function(size, graphicsScene, physicsEngine) {
    this.graphicsScene = graphicsScene;
    this.physicsEngine = physicsEngine;

    this.testRender = new TestRender(this.graphicsScene);

    // Container
    var backgroundContainer = new PIXI.DisplayObjectContainer();
    // Background
    var bg = PIXI.Texture.fromImage("/images/background.png");
    var tilingSprite = new PIXI.extras.TilingSprite(bg, windowSize.width, windowSize.height);
    // tilingSprite.z = 100;
    backgroundContainer.addChild(tilingSprite);

    // Background Filter
    var displacementSprite = PIXI.Sprite.fromImage("/images/water-filter.png");
    displacementSprite.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
    var displacementFilter = new PIXI.filters.DisplacementFilter(displacementSprite);
    backgroundContainer.addChild(displacementSprite);

    // construct object
    // scene ground
    var groundShift = 0;
    var partLeft = Bodies.rectangle(0 + groundShift, size.height / 2, 20, size.height + groundShift),
        partTop = Bodies.rectangle(size.width / 2, + groundShift, size.width + groundShift, 20, { render: partLeft.render }),
        partRight = Bodies.rectangle(size.width - groundShift, size.height / 2, 20, size.height + groundShift, { render: partLeft.render }),
        partBottom = Bodies.rectangle(size.width / 2, size.height - groundShift, size.width + groundShift, 20, { render: partLeft.render });

    this.ground = Body.create({
        parts: [partLeft, partTop, partRight, partBottom], isStatic: true
    });

    // add all of the bodies to the world
    World.add(this.physicsEngine.world, [this.ground]);

    this.graphicsScene.addChild(backgroundContainer);

    // var testWaveSprite = PIXI.Sprite.fromImage("/images/water-wave-filter.png");
    // testWaveSprite.anchor.x = 0.5;
    // testWaveSprite.anchor.y = 0.5;
    // testWaveSprite.x = windowSize.width / 2;
    // testWaveSprite.y = windowSize.height / 2 + 128;
    // testWaveSprite.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;
    // backgroundContainer.addChild(testWaveSprite);
    // this.graphicsScene.addChild(testWaveSprite);
    // var testWaveFilter = new PIXI.filters.DisplacementFilter(testWaveSprite);

    tilingSprite.filters = [displacementFilter/*, testWaveFilter*/];

    this.draw = function () {
        this.testRender.clearColor();
        for (var index in this.ground.parts) {
            this.testRender.renderBox(this.ground.parts[index]);
        }

        var offset = 0.4;

        displacementSprite.x += offset;
        displacementSprite.y += offset;

        // testWaveSprite.x += offset;
        // testWaveSprite.y += offset;
    }
}